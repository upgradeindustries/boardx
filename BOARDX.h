/*
 * Copyright (c) 2015 UPGRADE INDUSTRIES
 */
#ifndef _BOARDX_H
#define _BOARDX_H

#include <stdint.h>
#include <ByteBuffer.h>

#define LED_PIN 			(13) 			//SPI_CLOCK
#define INTERFACE_BAUD 		(9600)			//Length of messages from command app
#define MESSAGE_LENGTH 			(10)				//Length of messages from command app
#define ENCODED_MESSAGE_LENGTH 		(13)				//Length of messages from command app in base64 format
#define MESSAGE_DELIMITER 	(0xFF)			//signal that a message is ending

/*
*  Aux class to allow quick dumping of the buffer to a destination
*/
class ByteBufferPlus : public ByteBuffer{
	public:

		//return size of data in buffer up to and including buffer size
		uint8_t dataSize();

		//Returns length of data copied
		uint8_t copyData(uint8_t* dest);
		uint8_t copyData(uint8_t* dest, uint8_t length);

};

/* BoardX Motherboard library
 * Library written by UPGRADE INDUSTRIES
*/
class BOARDX {

	ByteBufferPlus read_buffer;
	int msg_count;

	public:
		BOARDX();

		//initialize communication with remote app
		void initApp();
		//prepare us to blink the LED on the BoardX motherboard
		void initLED();
		//1 = on, 0 = off
		void LED(uint8_t value);
		//Report all I2C Bus devices
		void scanI2C();
		//Returns 1 if received a command, 0 otherwise.
		//Saves message type in pointer and data		
		uint8_t receiveCommand(uint8_t* type, uint8_t* data);

};



#endif

