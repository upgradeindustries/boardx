#include <Arduino.h>
#include "BOARDX.h"
#include <stdint.h>
// #include "../Wire/Wire.h"
#include <Wire.h>
#include <Base64.h>	




BOARDX::BOARDX() : msg_count(), read_buffer(){
	read_buffer.init(MESSAGE_LENGTH);
  	read_buffer.clear();
}


//initialize communication with remote app
void BOARDX::initApp(){
	Serial.begin(INTERFACE_BAUD);
}


//prepare us to blink the LED on the BoardX motherboard
void BOARDX::initLED(){
	pinMode(LED_PIN,OUTPUT);
	digitalWrite(LED_PIN,0);
}


//1 = on, 0 = off
void BOARDX::LED(uint8_t value){
	digitalWrite(LED_PIN,value);
}


//Report all I2C Bus devices
void BOARDX::scanI2C(){

}

//Returns 1 if received a command, 0 otherwise.
//Saves message type in pointer and data		
uint8_t BOARDX::receiveCommand(uint8_t* type, uint8_t* data)
{
   uint8_t got_message = 0;
   while(Serial.available())
   {
	    uint8_t c = Serial.read();
	    
	    if(c == MESSAGE_DELIMITER)
	    {
	      
			got_message = 1;

			//save message type (zeroth byte, unencoded)
			*type = read_buffer.get();

			//dump it to the pointer			
			read_buffer.copyData(data);

			//de-base64-ify the remainder
			base64_decode(data,data,MESSAGE_LENGTH-2);

	      	msg_count++;
	  	}
	  	else
	  	{
	  		read_buffer.put(c);
	  	}
	}

	return got_message;

}

//Special buffer functions
uint8_t ByteBufferPlus::dataSize(){
	//getSize returns size of free space left, not size of data (hence the existence of this function)
	return getCapacity() - getSize();
}

uint8_t ByteBufferPlus::copyData(uint8_t* dest){
	return copyData(dest, dataSize());
}

uint8_t ByteBufferPlus::copyData(uint8_t* dest, uint8_t length){
	
	//sanity check for null buffer and data length zero
	if (dest == 0x0 || length == 0){
		return 0;
	}else{
		for(uint8_t i=0;i<length;i++){
			*dest++ = get();
		}
	}
}